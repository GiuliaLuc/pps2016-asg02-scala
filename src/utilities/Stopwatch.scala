package utilities

import java.util.concurrent.TimeUnit

/**
  * Created by lucch on 11/04/2017.
  */
object Stopwatch{
  def apply(): Stopwatch = new Stopwatch()
}
class Stopwatch() {
  private var running: Boolean = false
  private var startTime: Long = 0L

  def start() {
    running = true
    startTime = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis)
  }

  def stop() {
    startTime = getTime
    running = false
  }

  def getTime: Long = if (running) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis) - startTime else startTime
}