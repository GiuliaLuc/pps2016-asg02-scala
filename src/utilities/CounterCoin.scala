package utilities

/**
  * Created by lucch on 10/04/2017.
  */
object CounterCoin {

  var counter: Int = 0;

  def getCounter()= counter

  def increment() = counter+=1

}