package utilities

/**
  * Created by lucch on 11/04/2017.
  */
object ScoreGame {
    var score: Int = 0

    def getScore(): Int = score

    def amount(singleScore: Int): Unit =  score+=singleScore

}
