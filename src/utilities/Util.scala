package utilities

import javax.swing._
import java.awt._
import java.net.URL

/**
  * @author Roberto Casadei
  */
object Util {
  def getResource(path: String): URL = Util.getClass getResource(path)
  def getImage(path: String): Image = new ImageIcon(getResource(path)).getImage
}