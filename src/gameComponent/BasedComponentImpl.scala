package gameComponent

import java.awt.Image

import application.Application
import utilities.{ImagePath, Util}

object BasedComponentImpl{
  val WIDTH_BLOCK = 30
  val HEIGHT_BLOCK = 30
  val WIDTH_TUNNEL = 43
  val HEIGHT_TUNNEL = 65
}

class BasedComponentImpl(var x: Int, var y: Int, var width: Int, var height: Int, val imagePath: String) extends BasedComponent {
  private var image = Util.getImage(imagePath)

  def getWidth: Int = width

  def getHeight: Int = height

  def getX: Int = x

  def getY: Int = y

  def getImage: Image = image

  def setWidth(width: Int): Unit = this.width = width

  def setHeight(height: Int): Unit = this.height = height

  def setX(x: Int): Unit = this.x=x

  def setY(y: Int): Unit = this.y = y

  def setImage(imagePath: String): Unit = this.image = Util.getImage(imagePath)

  def move(): Unit =  if (Application.viewport.getxPos >= 0) { this.x = this.x - Application.viewport.getMov }
}

object Block{
  def apply(x: Int, y: Int): Block = new Block(x, y)
}

class Block(x: Int, y: Int) extends BasedComponentImpl(x, y, BasedComponentImpl.WIDTH_BLOCK, BasedComponentImpl.HEIGHT_BLOCK, ImagePath.IMG_BLOCK)

object Tunnel{
  def apply(x: Int, y: Int): Tunnel = new Tunnel(x, y)
}

class Tunnel( x: Int,  y: Int) extends BasedComponentImpl(x, y,BasedComponentImpl.WIDTH_TUNNEL, BasedComponentImpl.HEIGHT_TUNNEL, ImagePath.IMG_TUNNEL)