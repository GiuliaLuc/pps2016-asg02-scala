package gameComponent.characters

import java.awt.Image

import gameComponent.BasedComponentImpl
import utilities.{ImagePath, Util}

object BasicCharacter {
  private val PROXIMITY_MARGIN: Int = 10
  private val HIT_COSTANT: Int = 5
}

class BasicCharacter( x: Int, y: Int, width: Int, height: Int, imagePath: String) extends BasedComponentImpl(x, y, width, height, imagePath) with Character {
  private var moving: Boolean = false
  private var toRight: Boolean = true
  private var counter: Int = 0
  private var alive: Boolean = true

  def getCounter: Int = counter

  def isAlive: Boolean = alive

  def isMoving: Boolean = moving

  def isToRight: Boolean = toRight

  def setAlive(alive: Boolean): Unit =  this.alive = alive

  def setMoving(moving: Boolean): Unit = this.moving = moving

  def setToRight(toRight: Boolean): Unit = this.toRight = toRight

  def setCounter(counter: Int): Unit = this.counter = counter

  def walk(name: String, frequency: Int): Image = {
    val str: String = ImagePath.IMG_BASE + name +
      (if (!this.moving || ((this.getCounter + 1) % frequency) == 0) ImagePath.IMGP_STATUS_ACTIVE else ImagePath.IMGP_STATUS_NORMAL)+
      (if (this.toRight) ImagePath.IMGP_DIRECTION_DX else ImagePath.IMGP_DIRECTION_SX) + ImagePath.IMG_EXT

    return Util.getImage(str)
  }

  def hitAheadGameObject(gameComponent: BasedComponentImpl): Boolean = !(this.getX + this.getWidth < gameComponent.getX || this.getX + this.getWidth > gameComponent.getX + BasicCharacter.HIT_COSTANT || this.getY + this.getHeight <= gameComponent.getY || this.getY >= gameComponent.getY + gameComponent.getHeight)

  def hitAhead(character: BasicCharacter): Boolean = if (this.isToRight) hitAheadGameObject(character) else false

  def hitBack(gameComponent: BasedComponentImpl): Boolean = !(this.getX > gameComponent.getX + gameComponent.getWidth || this.getX + this.getWidth < gameComponent.getX + gameComponent.getWidth - BasicCharacter.HIT_COSTANT || this.getY + this.getHeight <= gameComponent.getY || this.getY >= gameComponent.getY + gameComponent.getHeight)

  def hitBelow(gameComponent: BasedComponentImpl): Boolean = !(this.getX + this.getWidth < gameComponent.getX + 5 || this.getX > gameComponent.getX + gameComponent.getWidth - BasicCharacter.HIT_COSTANT || this.getY + this.getHeight < gameComponent.getY || this.getY + this.getHeight > gameComponent.getY + BasicCharacter.HIT_COSTANT)

  def hitBelow(character: BasicCharacter): Boolean = !(this.getX + this.getWidth < character.getX || this.getX > character.getX + character.getWidth || this.getY + this.getHeight < character.getY || this.getY + this.getHeight > character.getY)

  def hitAbove(gameComponent: BasedComponentImpl): Boolean = !(this.getX + this.getWidth < gameComponent.getX + 5 || this.getX > gameComponent.getX + gameComponent.getWidth - BasicCharacter.HIT_COSTANT || this.getY < gameComponent.getY + gameComponent.getHeight || this.getY > gameComponent.getY + gameComponent.getHeight + BasicCharacter.HIT_COSTANT)

  def isNearby(gameComponent: BasedComponentImpl): Boolean = (this.getX > gameComponent.getX - BasicCharacter.PROXIMITY_MARGIN && this.getX < gameComponent.getX + gameComponent.getWidth + BasicCharacter.PROXIMITY_MARGIN) || (this.getX + this.getWidth > gameComponent.getX - BasicCharacter.PROXIMITY_MARGIN && this.getX + this.getWidth < gameComponent.getX + gameComponent.getWidth + BasicCharacter.PROXIMITY_MARGIN)
}