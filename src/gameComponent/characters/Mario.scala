package gameComponent.characters

import java.awt.Image

import application.Application
import game.Audio
import gameComponent.BasedComponentImpl
import gameComponent.objects.Coin
import utilities.{ImagePath, Util}

object Mario {
  private val MARIO_OFFSET_Y_INITIAL: Int = 243
  private val FLOOR_OFFSET_Y_INITIAL: Int = 293
  private val WIDTH: Int = 28
  private val HEIGHT: Int = 50
  private val JUMPING_LIMIT: Int = 42
}

class Mario(x: Int, y: Int) extends BasicCharacter(x, y, Mario.WIDTH, Mario.HEIGHT, ImagePath.IMG_MARIO_DEFAULT) {
  private var jumping: Boolean = false
  private var jumpingExtent: Int = 0

  def isJumping: Boolean = jumping

  def setJumping(jumping: Boolean): Unit = this.jumping = jumping

  def doJump(): Image = {
    var imagePath: String = null
    this.jumpingExtent += 1
    if (this.jumpingExtent < Mario.JUMPING_LIMIT) {
      if (this.getY > Application.viewport.getHeightLimit) this.setY(this.getY - 4) else  this.jumpingExtent = Mario.JUMPING_LIMIT
      imagePath = if (this.isToRight) ImagePath.IMG_MARIO_SUPER_DX  else ImagePath.IMG_MARIO_SUPER_SX
    } else {
      if (this.getY + this.getHeight < Application.viewport.getFloorOffsetY) {
        this.setY(this.getY + 1)
        imagePath = if (this.isToRight) ImagePath.IMG_MARIO_SUPER_DX else ImagePath.IMG_MARIO_SUPER_SX
      }else {
        imagePath = if (this.isToRight) ImagePath.IMG_MARIO_ACTIVE_DX else ImagePath.IMG_MARIO_ACTIVE_SX
        this.jumping = false
        this.jumpingExtent = 0
      }
    }
    Util.getImage(imagePath)
  }

  def contactGameComponent(gameComponent: BasedComponentImpl) {
    if (this.hitAheadGameObject(gameComponent) && this.isToRight || this.hitBack(gameComponent) && !this.isToRight) {
      Application.viewport.setMov(0)
      this.setMoving(false)
    }
    if (this.hitBelow(gameComponent) && this.jumping) {
      Application.viewport.setFloorOffsetY(gameComponent.getY)
    } else {
      if (!this.hitBelow(gameComponent)) {
        Application.viewport.setFloorOffsetY(Mario.FLOOR_OFFSET_Y_INITIAL)
        if (!this.jumping) this.setY(Mario.MARIO_OFFSET_Y_INITIAL)
        if (hitAbove(gameComponent)) {
          Application.viewport.setHeightLimit(gameComponent.getY + gameComponent.getHeight)
        }else{
          if (!this.hitAbove(gameComponent) && !this.jumping) Application.viewport.setHeightLimit(0)
        }
      }
    }
  }

  def contactPiece(coin: Coin): Boolean =  this.hitBack(coin) || this.hitAbove(coin) || this.hitAheadGameObject(coin) || this.hitBelow(coin)

  def contactCharacter(character: BasicCharacter) {
    if (this.hitAhead(character) || this.hitBack(character)) {
      if (character.isAlive) {
        this.setMoving(false)
        this.setAlive(false)
      } else {
        this.setAlive(true)
      }
    }else if (this.hitBelow(character)) {
        character.setMoving(false)
        character.setAlive(false)
      }
  }
}