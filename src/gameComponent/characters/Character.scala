package gameComponent.characters

import java.awt.Image

import gameComponent.BasedComponentImpl

trait Character {
  def getCounter: Int

  def isAlive: Boolean

  def isMoving: Boolean

  def isToRight: Boolean

  def setAlive(alive: Boolean)

  def setMoving(moving: Boolean)

  def setToRight(toRight: Boolean)

  def setCounter(counter: Int)

  def walk(name: String, frequency: Int): Image

  def hitAheadGameObject(og: BasedComponentImpl): Boolean

  def hitAhead(character: BasicCharacter): Boolean

  def hitBack(og: BasedComponentImpl): Boolean

  def hitBelow(og: BasedComponentImpl): Boolean

  def hitBelow(character: BasicCharacter): Boolean

  def hitAbove(og: BasedComponentImpl): Boolean

  def isNearby(obj: BasedComponentImpl): Boolean
}