package gameComponent.characters

import java.awt.Image

import gameComponent.BasedComponentImpl
import utilities.{ImagePath, Util}

/**
  * Created by lucch on 09/04/2017.
  */

object Enemies{
  private val WIDTH_TURTLE: Int = 43
  private val HEIGHT_TURTLE: Int = 50
  private val WIDTH_MUSHROOM: Int = 27
  private val HEIGHT_MUSHROOM: Int = 30


  private val PAUSE: Int = 15
}

class Enemies(x: Int, y: Int, width: Int, height: Int, image: String, imageDDx: String, imageDSX: String) extends BasicCharacter(x, y, width, height, image) with Runnable {

  private var offsetX: Int = 0
  this.setToRight(true)
  this.setMoving(true)
  val chronoEnemies: Thread = new Thread(this)
  chronoEnemies.start()

  case class Mushroom() extends Enemies(x, y, Enemies.WIDTH_MUSHROOM, Enemies.HEIGHT_MUSHROOM, ImagePath.IMG_MUSHROOM_DEFAULT, ImagePath.IMG_MUSHROOM_DEAD_DX,ImagePath.IMG_MUSHROOM_DEAD_SX)
  case class Turtle() extends Enemies(x, y, Enemies.WIDTH_TURTLE, Enemies.HEIGHT_TURTLE, ImagePath.IMG_TURTLE_IDLE, ImagePath.IMG_TURTLE_DEAD, ImagePath.IMG_TURTLE_DEAD)

  override def move(): Unit = {
    this.offsetX = if (isToRight) 1
    else -1
    this.setX(this.getX + this.offsetX)
  }

  override def run(): Unit = {
    while (true) {
      if (this.isAlive) {
        this.move()
        try {
          Thread.sleep(Enemies.PAUSE)
        } catch {
          case e: InterruptedException => {}
        }
      }
    }
  }
  def contact(obj: BasedComponentImpl): Unit = {
    if (this.hitAheadGameObject(obj) && this.isToRight) {
      this.setToRight(false)
      this.offsetX = -1
    } else if (this.hitBack(obj) && !this.isToRight) {
      this.setToRight(true)
      this.offsetX = 1
    }
  }

  def contact(pers: BasicCharacter): Unit = {
    if (this.hitAhead(pers) && this.isToRight) {
      this.setToRight(false)
      this.offsetX = -1
    }else if (this.hitBack(pers) && !this.isToRight) {
      this.setToRight(true)
      this.offsetX = 1
    }
  }

  def deadImage: Image = Util.getImage(if (this.isToRight) imageDDx else imageDSX)

}