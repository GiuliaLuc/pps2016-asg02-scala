package gameComponent

import java.awt._

/**
  * Created by lucch on 17/03/2017.
  */
trait BasedComponent {
  def getWidth: Int

  def getHeight: Int

  def getX: Int

  def getY: Int

  def getImage: Image

  def setWidth(width: Int)

  def setHeight(height: Int)

  def setX(x: Int)

  def setY(y: Int)

  def setImage(imagePath: String)

  def move()
}