package gameComponent.objects

import java.awt.Image

import gameComponent.BasedComponentImpl
import utilities.{ImagePath, Util}

object Coin {
  private val WIDTH: Int = 30
  private val HEIGHT: Int = 30
  private val PAUSE: Int = 10
  private val FLIP_FREQUENCY: Int = 100

  def apply(x: Int, y: Int): Coin = new Coin(x, y)
}

class Coin(x: Int, y: Int) extends BasedComponentImpl(x, y,Coin.WIDTH , Coin.HEIGHT, ImagePath.IMG_PIECE1) with Runnable {
  private var counter: Int = 0

  def imageOnMovement: Image =
    Util.getImage(if ( {this.counter += 1; this.counter}% Coin.FLIP_FREQUENCY == 0) ImagePath.IMG_PIECE1 else ImagePath.IMG_PIECE2)

  override def run(): Unit = {
    while (true) {
       this.imageOnMovement
       try {
         Thread.sleep(Coin.PAUSE)
       }catch {
         case e: InterruptedException => {}
       }

    }
  }
}
