package application

import game.{PlatformImpl, Refresh}
import javax.swing.JFrame

object Application {
  private val WINDOW_WIDTH: Int = 700
  private val WINDOW_HEIGHT: Int = 360
  private val WINDOW_TITLE: String = "Super Mario"
  var viewport: PlatformImpl = null

  def main(args: Array[String]) {
    val mainFrame: JFrame = new JFrame(WINDOW_TITLE)
    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    mainFrame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT)
    mainFrame.setLocationRelativeTo(null)
    mainFrame.setResizable(true)
    mainFrame.setAlwaysOnTop(true)
    viewport = PlatformImpl.getInstance
    mainFrame.setContentPane(viewport)
    mainFrame.setVisible(true)
    val timer: Thread = new Thread(Refresh.apply)
    timer.start()
  }
}