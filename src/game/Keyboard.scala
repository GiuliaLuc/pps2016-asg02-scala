package game

import java.awt.event.{KeyEvent, KeyListener}

class Keyboard extends KeyListener {
  def keyPressed(e: KeyEvent): Unit = {
    val platform: PlatformImpl = PlatformImpl.getInstance
    if (platform.getMario.isAlive) {
      if (e.getKeyCode == KeyEvent.VK_RIGHT) {
        if (platform.getxPos == -1) {
          platform.setxPos(0)
          platform.setBackground1PosX(-50)
          platform.setBackground2PosX(750)
        }
        platform.getMario.setMoving(true)
        platform.getMario.setToRight(true)
        platform.setMov(1)
      }
      else if (e.getKeyCode == KeyEvent.VK_LEFT) {
        if (platform.getxPos == 4601) {
          platform.setxPos(4600)
          platform.setBackground1PosX(-50)
          platform.setBackground2PosX(750)
        }
        platform.getMario.setMoving(true)
        platform.getMario.setToRight(false)
        platform.setMov(-1)
      }
      if (e.getKeyCode == KeyEvent.VK_UP) {
        platform.getMario.setJumping(true)
        Audio.playSound("/resources/audio/jump.wav")
      }
    }
  }

  def keyReleased(e: KeyEvent): Unit = {
    val platform: PlatformImpl = PlatformImpl.getInstance
    platform.getMario.setMoving(false)
    platform.setMov(0)
  }

  def keyTyped(e: KeyEvent): Unit = {
  }
}