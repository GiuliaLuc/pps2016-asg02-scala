package game

import javax.sound.sampled.{AudioInputStream, AudioSystem, Clip}

object Audio {

  def playSound(son: String): Unit = {
    val s: Audio = new Audio(son)
    s.play()
  }

}

class Audio ( song: String) {
  private var clip: Clip = null
  try {
    val audio: AudioInputStream = AudioSystem.getAudioInputStream(getClass.getResource(song))
    clip = AudioSystem.getClip
    clip.open(audio)
  }
  catch {
    case e: Exception => {}
  }

  def play(): Unit = clip.start()
}