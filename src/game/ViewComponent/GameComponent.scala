package game.ViewComponent

import java.util.{ArrayList, List}

import gameComponent.{BasedComponentImpl, Block, Tunnel}
import gameComponent.objects.Coin

/**
  * Created by lucch on 18/03/2017.
  */
trait GameComponent {
  def getTunnelList: List[Tunnel]

  def getBlockList: List[Block]

  def getCoinList: List[Coin]

  def getComponents: List[BasedComponentImpl]
}

class GameComponentImpl() extends GameComponent {
  private val components: List[BasedComponentImpl] = new ArrayList[BasedComponentImpl]
  private val coins: List[Coin] = new ArrayList[Coin]
  private val tunnelList: List[Tunnel] = new ArrayList[Tunnel]
  private val blockList: List[Block] = new ArrayList[Block]

  def getTunnelList: List[Tunnel] = {
    this.tunnelList.add(Tunnel(600, 230))
    this.tunnelList.add(Tunnel(1000, 230))
    this.tunnelList.add(Tunnel(1600, 230))
    this.tunnelList.add(Tunnel(1900, 230))
    this.tunnelList.add(Tunnel(2500, 230))
    this.tunnelList.add(Tunnel(3000, 230))
    this.tunnelList.add(Tunnel(3800, 230))
    this.tunnelList.add(Tunnel(4500, 230))
    tunnelList
  }

  def getBlockList: List[Block] = {
    this.blockList.add(Block(400, 180))
    this.blockList.add(Block(1200, 180))
    this.blockList.add(Block(1270, 170))
    this.blockList.add(Block(1340, 160))
    this.blockList.add(Block(2000, 180))
    this.blockList.add(Block(2600, 160))
    this.blockList.add(Block(2650, 180))
    this.blockList.add(Block(3500, 160))
    this.blockList.add(Block(3550, 140))
    this.blockList.add(Block(4000, 170))
    this.blockList.add(Block(4200, 200))
    this.blockList.add(Block(4300, 210))
    blockList
  }

  def getCoinList: List[Coin] = {
    this.coins.add(Coin(402, 145))
    this.coins.add(Coin(1202, 140))
    this.coins.add(Coin(1272, 95))
    this.coins.add(Coin(1342, 40))
    this.coins.add(Coin(1650, 145))
    this.coins.add(Coin(2650, 145))
    this.coins.add(Coin(3000, 135))
    this.coins.add(Coin(3400, 125))
    this.coins.add(Coin(4200, 145))
    this.coins.add(Coin(4600, 40))
    this.coins
  }

  def getComponents: List[BasedComponentImpl] = {
    this.components.addAll(this.getTunnelList)
    this.components.addAll(this.getBlockList)
    this.components
  }
}