package game.ViewComponent

import java.awt._

import utilities.{ImagePath, Util}

/**
  * Created by lucch on 18/03/2017.
  */
class ImageToBackground() {
  private val imageBackground1: Image = Util.getImage(ImagePath.IMG_BACKGROUND)
  private val imageBackground2: Image = Util.getImage(ImagePath.IMG_BACKGROUND)
  private val castle: Image = Util.getImage(ImagePath.IMG_CASTLE)
  private val startImage: Image = Util.getImage(ImagePath.START_ICON)
  private val imageCastle: Image = Util.getImage(ImagePath.IMG_CASTLE_FINAL)
  private val imageFlag: Image = Util.getImage(ImagePath.IMG_FLAG)

  def getImageBackground1: Image = imageBackground1

  def getImageBackground2: Image = imageBackground2

  def getCastle: Image = castle

  def getStartImage: Image = startImage

  def getImageCastle: Image = imageCastle

  def getImageFlag: Image = imageFlag
}