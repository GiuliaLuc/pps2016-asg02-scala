package game

import game.ViewComponent.GameComponent
import game.ViewComponent.GameComponentImpl
import game.ViewComponent.ImageToBackground
import gameComponent.BasedComponentImpl
import gameComponent.characters.Enemies
import gameComponent.characters.Mario
import gameComponent.objects.Coin
import utilities._
import javax.swing._
import java.awt._
import java.util.List

object PlatformImpl {
  private var platformSingleton: PlatformImpl = null

  def getInstance: PlatformImpl = {
    if (platformSingleton == null) {
      platformSingleton = new PlatformImpl
    }
    return platformSingleton
  }

  private val MARIO_FREQUENCY: Int = 25
  private val MUSHROOM_FREQUENCY: Int = 45
  private val TURTLE_FREQUENCY: Int = 45
  private val MUSHROOM_DEAD_OFFSET_Y: Int = 20
  private val TURTLE_DEAD_OFFSET_Y: Int = 30
  private val FLAG_X_POS: Int = 4650
  private val CASTLE_X_POS: Int = 4850
  private val FLAG_Y_POS: Int = 115
  private val CASTLE_Y_POS: Int = 145
}
class PlatformImpl private() extends JPanel with Platform {
  private val  gameComponent: GameComponent = new GameComponentImpl
  private val imageToBackground: ImageToBackground = new ImageToBackground()
  private val stopwatch: Stopwatch = Stopwatch()
  private val mario: Mario = new Mario(300, 245)
  private val mushroom: Enemies = new Enemies(800, 263, 43, 50, ImagePath.IMG_MUSHROOM_DEFAULT, ImagePath.IMG_MUSHROOM_DEAD_DX, ImagePath.IMG_MUSHROOM_DEAD_SX)
  private val turtle: Enemies = new Enemies(950, 243, 27, 30, ImagePath.IMG_MUSHROOM_DEFAULT, ImagePath.IMG_MUSHROOM_DEAD_DX, ImagePath.IMG_MUSHROOM_DEAD_SX)
  private val components: List[BasedComponentImpl] = gameComponent.getComponents
  private val coins: List[Coin] = gameComponent.getCoinList
  private var background1PosX: Int = -50
  private var background2PosX: Int = 750
  private var mov: Int = 0
  private var xPos: Int = -1
  private var floorOffsetY: Int = 293
  private var heightLimit: Int = 0
  var gameover : Int = 0

  this.setFocusable(true)
  this.requestFocusInWindow
  this.addKeyListener(new Keyboard)
  stopwatch.start()


  def getMario: Mario = mario

  def getFloorOffsetY: Int = floorOffsetY

  def getHeightLimit: Int = heightLimit

  def getMov: Int = mov

  def getxPos: Int = xPos

  def setBackground2PosX(background2PosX: Int) = this.background2PosX = background2PosX

  def setFloorOffsetY(floorOffsetY: Int): Unit = this.floorOffsetY = floorOffsetY

  def setHeightLimit(heightLimit: Int): Unit = this.heightLimit = heightLimit

  def setxPos(xPos: Int): Unit = this.xPos = xPos

  def setMov(mov: Int): Unit = this.mov = mov

  def setBackground1PosX(x: Int): Unit = this.background1PosX = x

  def updateBackgroundOnMovement() {
    if (this.xPos >= 0 && this.xPos <= 4600) {
      this.xPos = this.xPos + this.mov
      this.background1PosX = this.background1PosX - this.mov
      this.background2PosX = this.background2PosX - this.mov
    }
    if (this.background1PosX == -800) {
      this.background1PosX = 800
    }else if (this.background2PosX == -800) {
      this.background2PosX = 800
    }else if (this.background1PosX == 800) {
      this.background1PosX = -800
    }else if (this.background2PosX == 800) {
      this.background2PosX = -800
    }
  }



  override def paintComponent(g: Graphics) {
    super.paintComponent(g)
    val g2: Graphics = g.asInstanceOf[Graphics2D]
    var i: Int = 0
    while (i < components.size) {
      if (this.mario.isNearby(this.components.get(i))) this.mario.contactGameComponent(this.components.get(i))
      if (this.mushroom.isNearby(this.components.get(i))) this.mushroom.contact(this.components.get(i))
      if (this.turtle.isNearby(this.components.get(i))) this.turtle.contact(this.components.get(i))
      i += 1; i - 1
    }
    i=0
    while (i < coins.size) {

      if (this.mario.contactPiece(this.coins.get(i))) {
        Audio.playSound(ImagePath.AUDIO_MONEY)
        this.coins.remove(i)
        CounterCoin.increment()
        ScoreGame.amount(200)
      }
      i += 1; i - 1

    }
    if (this.mushroom.isNearby(turtle)) {
      this.mushroom.contact(turtle)
    }
    if (this.turtle.isNearby(mushroom)) {
      this.turtle.contact(mushroom)
    }
    if (this.mario.isNearby(mushroom)) {
      this.mario.contactCharacter(mushroom)
      ScoreGame.amount(100)
    }
    if (this.mario.isNearby(turtle)) {
      this.mario.contactCharacter(turtle)
      ScoreGame.amount(200)
    }
    this.updateBackgroundOnMovement()
    if (this.xPos >= 0 && this.xPos <= 4600) {
      i=0
      while (i < components.size) {
        {
          components.get(i).move()
        }
        {
          i += 1; i - 1
        }
      }
      i=0
      while (i < coins.size) {
        {
          this.coins.get(i).move()
        }
        {
          i += 1; i - 1
        }
      }
      this.mushroom.move()
      this.turtle.move()
    }
    if (!this.mario.isAlive) {
      stopwatch.stop()
      if(gameover==2){
        Thread.sleep(2600)
        System.exit(0)
      }
      gameover=1;
      if(gameover == 1){
        try {
          Audio.playSound("/resources/audio/gameover.wav")
          g2.setColor(Color.BLACK)
          g2.fill3DRect(0, 0, 800, 800, true)
          g2.drawImage(Util.getImage(ImagePath.GAME_OVER), 0, 0, null)
          gameover=2
        }catch {
          case x: Exception => {}
        }}
    }
    else {
      g2.drawImage(imageToBackground.getImageBackground1, this.background1PosX, 0, null)
      g2.drawImage(imageToBackground.getImageBackground2, this.background2PosX, 0, null)
      g2.drawImage(imageToBackground.getCastle, 10 - this.xPos, 95, null)
      g2.drawImage(imageToBackground.getStartImage, 220 - this.xPos, 234, null)
      g2.drawImage(Util.getImage(ImagePath.IMG_PIECE1), 8, 4, null)
      g2.setFont( new Font( "SansSerif", Font.BOLD, 20 ) )
      g2.drawString(String.valueOf("X " + CounterCoin.getCounter), 40, 26)
      g2.setFont( new Font( "SansSerif", Font.TYPE1_FONT, 18 ) )
      g2.drawString(String.valueOf("SCORE " + ScoreGame.getScore()), 280, 26)
      g2.setFont( new Font( "SansSerif", Font.TYPE1_FONT, 18 ) )
      g2.drawString(String.valueOf("TIMER " + stopwatch.getTime + " sec"), 550, 26)
      i=0
      while (i < components.size) {
        {
          g2.drawImage(this.components.get(i).getImage, this.components.get(i).getX, this.components.get(i).getY, null)
        }
        {
          i += 1; i - 1
        }
      }
      i=0
      while (i < coins.size) {
        {
          g2.drawImage(this.coins.get(i).imageOnMovement, this.coins.get(i).getX, this.coins.get(i).getY, null)
        }
        {
          i += 1; i - 1
        }
      }
      g2.drawImage(imageToBackground.getImageFlag, PlatformImpl.FLAG_X_POS - this.xPos, PlatformImpl.FLAG_Y_POS, null)
      g2.drawImage(imageToBackground.getImageCastle, PlatformImpl.CASTLE_X_POS - this.xPos, PlatformImpl.CASTLE_Y_POS, null)
      if (this.mario.isJumping) {
        g2.drawImage(this.mario.doJump, this.mario.getX, this.mario.getY, null)
      }
      else g2.drawImage(this.mario.walk(ImagePath.IMGP_CHARACTER_MARIO, PlatformImpl.MARIO_FREQUENCY), this.mario.getX, this.mario.getY, null)
      if (this.mushroom.isAlive) {
        g2.drawImage(this.mushroom.walk(ImagePath.IMGP_CHARACTER_MUSHROOM, PlatformImpl.MUSHROOM_FREQUENCY), this.mushroom.getX, this.mushroom.getY, null)
      }
      else g2.drawImage(this.mushroom.deadImage, this.mushroom.getX, this.mushroom.getY + PlatformImpl.MUSHROOM_DEAD_OFFSET_Y, null)
      if (this.turtle.isAlive) {
        g2.drawImage(this.turtle.walk(ImagePath.IMGP_CHARACTER_TURTLE, PlatformImpl.TURTLE_FREQUENCY), this.turtle.getX, this.turtle.getY, null)
      }
      else g2.drawImage(this.turtle.deadImage, this.turtle.getX, this.turtle.getY + PlatformImpl.TURTLE_DEAD_OFFSET_Y, null)
    }
  }
}