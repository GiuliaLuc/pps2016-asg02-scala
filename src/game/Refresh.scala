package game

import utilities.Stopwatch

object Refresh{
  def apply: Refresh = new Refresh()
}

class Refresh extends Runnable {
  final private val PAUSE: Int = 3

  override def run() {
    val platform: PlatformImpl = PlatformImpl.getInstance
    val stopwatch: Stopwatch = Stopwatch()
    while (true) {
      platform.repaint()
      try {
        Thread.sleep(PAUSE)
      }catch {
          case e: InterruptedException => {}
      }
    }
  }
}