package game

import gameComponent.characters.Mario
import java.awt._

/**
  * Created by lucch on 18/03/2017.
  */
trait Platform {
  def getMario: Mario

  def getFloorOffsetY: Int

  def getHeightLimit: Int

  def getMov: Int

  def getxPos: Int

  def setBackground2PosX(background2PosX: Int)

  def setFloorOffsetY(floorOffsetY: Int)

  def setHeightLimit(heightLimit: Int)

  def setxPos(xPos: Int)

  def setMov(mov: Int)

  def setBackground1PosX(x: Int)

  def updateBackgroundOnMovement()

  def paintComponent(g: Graphics)
}